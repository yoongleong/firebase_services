# firebase_services

Skeleton project for firebase functions

## Prerequisites

https://firebase.google.com/docs/functions/get-started

## Structure

src/functions
- cloud functions need extension "fun.ts"

src/common
- this is intended to be a commons library for reusable code

src/model
- common package to store models for firestore documents

test/*
- unit tests need extension "test.ts"

## Firebase Functions index

The index automatically compiles and exports functions based on the directory they are in.

```
index.ts 
```

For example,
```
src/functions/http/message/hello.fun.ts
src/functions/userPurchases/onCreate.fun.ts
```

will be exported as
```
httpMessageHello
userPurchasesOnCreate
```

The convention is to have a similar structure as the firestore structure and name functions based on their triggers.

## Samples 

Sample http trigger function, with typescript (and strong typing) :
```
src/functions/http/message/hello.fun.ts
```

Sample unit test for above (which benefits from loose typing):
```
test/functions/http/message/hello.test.ts
```

*Note:* I use VSC which seems to have decent auto-complete out of the box.

## TODO 

- Sample firebase trigger fuction with write & unit tests

- ..

## Installing

```
$ cd functions
```

Install module dependencies within package.json
```
$ npm install
```

## Running the tests

```
$ npm test
```

this generates the following :

- coverage report, coverage/index.html

- unit test report, mochawesome-report/mochawesome.html (mochawesome might be overkill - seems more 'pretty' than functional but we can easily swap out for another reporter later)


## Deployment

see firebase documentation


## Built With

"Cloud Functions runs Node v.6.11.5, so we recommend that you develop locally with this version."
