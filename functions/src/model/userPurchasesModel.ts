export class UserPurchase {

  readonly platform: string;
  readonly receipt_id: string;
  readonly local_currency: string;
  readonly item_type : string;
  readonly items : string[];
  readonly purchased_at : number;
  readonly created_at : number;
  
  purchase_id: string;
  state: string;
  expiry_at: string;

  last_validated_at : number;
  last_updated_at : number;

  constructor(data) {
    this.platform = data.platform
    this.receipt_id = data.receipt_id
    this.local_currency = data.local_currency
    this.item_type = data.item_type
    this.items = data.items
    this.purchased_at = data.purchased_at
    this.created_at = data.created_at
  }

}