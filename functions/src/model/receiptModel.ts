export class Receipt {

  readonly platform: string;
  readonly apple_receipt: string;
  readonly apple_transaction_id : string;
  readonly android_token : string;
  readonly android_package_name : string;
  readonly android_subscription_id : string;
  readonly android_product_id : string;

  constructor(data) {
    this.platform = data.platform;
    this.apple_receipt = data.apple_receipt;
    this.apple_transaction_id = data.apple_transaction_id;
    this.android_token = data.android_token;
    this.android_package_name = data.android_package_name;
    this.android_subscription_id = data.android_subscription_id;
    this.android_product_id = data.android_product_id;
  }

}