export enum PurchaseItemType {
  COURSE_FREE,

  COURSE_RENTAL_10_DAY,
  COURSE_PURCHASE_10_DAY,
  COURSE_BALANCE_10_DAY,

  COURSE_RENTAL_30_DAY,
  COURSE_PURCHASE_30_DAY,
  COURSE_BALANCE_30_DAY,

  // Melchior
  MELCHIOR_1,
  MELCHIOR_2,
  MELCHIOR_3
}

export enum PurchaseItemState {
  ACTIVE,
  INACTIVE,
  INVALID,
  FAILED
}

export enum PurchasePlatform {
  APPLE,
  ANDROID,
  INSIGHT
}

export enum PurchaseHistoryEvent {
  COURSE_NEW,
  COURSE_COMPLETED,
  COURSE_EXPIRED,
  SUBSCRIPTION_NEW,
  SUBSCRIPTION_EXPIRED,
  SUBSCRIPTION_RENEWED,
  SUBSCRIPTION_TERMINATED,
  SUBSCRIPTION_CANCELLED,
  GIFT_GIVEN,
  GIFT_EXPIRED,
  VALIDATE_RETRY,
  VALIDATE_SUCCESS,
  NONE,
}

export class PurchaseRecord {

  // from user_purchase :
  client_purchase_id: string;
  platform: PurchasePlatform;
  user_id: string;
  receipt_id: string;
  local_currency: string;
  item_type: PurchaseItemType;
  items: string[];

  purchased_at: Date;
  created_at: Date;

  // filled by cloud functions :
  state: PurchaseItemState;
  expiry_at?: Date = null;

  last_validated_at: Date;
  last_updated_at: Date;
  last_event: PurchaseHistoryEvent;

  receipt_hash: Date;
  is_test_account: boolean;
  is_duplicate: boolean;

}