import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { Firestore } from "@google-cloud/firestore";

function initialize(): void {
    admin.initializeApp(functions.config().firebase);
  }

function getFirestore(): Firestore {
    return admin.firestore();
  }

module.exports.initialize = initialize
module.exports.getFirestore = getFirestore
