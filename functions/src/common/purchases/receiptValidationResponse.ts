
export class ReceiptValidationResponse {

  expiryDate? : number = null;
  cancellationDate? : number = null;
  isValid : boolean;
  isFound : boolean ;
  isTestAccount : boolean;
  cancelReason? : string = null;

}
