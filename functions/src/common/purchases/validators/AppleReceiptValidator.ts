import { Receipt } from "../../../model/receiptModel"
import { ReceiptValidationResponse } from "../receiptValidationResponse"
import { PurchaseItemState } from "../../../model/purchasesModel"
import { AppConfig } from "../../../config/config"
import { ReceiptValidator, PromiseCallback } from "./receiptValidators"

let config: AppConfig = require("../../../config/config")

/**
 * TODO - fake apple receipts implementation
 */

function maybeBase64(receipt: string): string {
  const rectified = receipt.replace(/\\/g, "");
  return Buffer.from(rectified, 'base64').toString('base64');
}

const errorMap = {
  21000: '21000 - The App Store could not read the JSON object you provided.',
  21002: '21002 - The data in the receipt-data property was malformed.',
  21003: '21003 - The receipt could not be authenticated.',
  21004: '21004 - The shared secret you provided does not match the shared secret on file for your account.',
  21005: '21005 - The receipt server is not currently available.',
  21006: '21006 - This receipt is valid but the subscription has expired. When this status code is returned to your server, the receipt data is also decoded and returned as part of the response.',
  21007: '21007 - This receipt is a sandbox receipt, but it was sent to the production service for verification.',
  21008: '21008 - This receipt is a production receipt, but it was sent to the sandbox service for verification.',
  21010: '21010 - This receipt could not be authorized. Treat this the same as if a purchase was never made.',
  21100: '21100 - Internal data access error.'
}

const cancelMap = {
  1: "Customer canceled their subscription.",
  2: "Billing error; for example customer's payment information was no longer valid." ,
  3: "Customer did not agree to recent price increase.",
  4: "Product was not available for purchase at the time of renewal.",
  5: "Unknown error."
}

class AppleValidationResponse {
  status: number
  receipt: AppleReceiptData
  latest_receipt_info: Array<AppleInAppData>
  pending_renewal_info: Array<ApplePendingRenewalInfo>

  test?: boolean = false
}

class AppleReceiptData {
  application_version: string
  bundle_id: string
  receipt_creation_date_ms: string
  original_purchase_date_ms: string
  in_app: Array<AppleInAppData>
}

class AppleInAppData {
  product_id: string
  transaction_id: string
  original_transaction_id: string
  expires_date_ms: string
  purchase_date_ms: string
}

class ApplePendingRenewalInfo {
  expiration_intent: string
  product_id: string
  auto_renew_produce_id: string
  auto_renew_status: string
  is_in_billing_retry_period: string
}

let appleProd = 'https://buy.itunes.apple.com/verifyReceipt';
let appleSandbox = 'https://sandbox.itunes.apple.com/verifyReceipt';

function callAppleApi(receipt: string, validation, callback): Promise<AppleValidationResponse> {

  var contents = {
    'receipt-data': maybeBase64(receipt),
    'password': config.receiptValidation.applePassword,
    'exclude-old-transactions': false
  }

  var test = false;
  const request = require("request-promise");
  return request.post({ url: appleProd, body: contents, json: true })
    .then((res) => {
      if (res.status === 21007) {
        test = true;
        return request.post({ url: appleSandbox, body: contents, json: true })
      } else {
        return res;
      }

    }).then((res) => {
      return Object.assign({}, res, { "test": test });

    }).then((res) => {
      
      if (res.status === 0) {
        return validation(res);
      }

      switch (res.status) {
        case 21000:
        case 21002:
          // INVALID
          return callback(
            Promise.resolve(
              { isValid: false, isFound: false, isTestAccount: res.test }
            ));

        case 21003:
        case 21010:
          // FAIL 
          return callback(
            Promise.resolve(
              { isValid: true, isFound: false, isTestAccount: res.test }
            ));

        default:
          // RETRY
          return callback(Promise.reject(TypeError("sandbox=" + test + ", " + errorMap[res.status])));
      }

    })
    .catch((err) => {
      // RETRY
      console.warn(`Caught error validating receipt - sandbox=${test}, ${err}`)
      return callback(Promise.reject(TypeError(err.message)));
    });;
}

type ValidationCallback = (response: AppleValidationResponse) => any;

export class AppleReceiptValidator implements ReceiptValidator {

  constructor() {
    console.log("initializing apple validator");
  }

  validatePurchase(receipt: Receipt, callback: PromiseCallback) {

    var validation: ValidationCallback = (res) => {
      if (res.receipt.in_app.some(r => r.original_transaction_id == receipt.apple_transaction_id)) {
        return callback(
          Promise.resolve(
            { isValid: true, isFound: true, isTestAccount: res.test }
          ));
      } else {
        return callback(
          Promise.resolve(
            { isValid: true, isFound: false, isTestAccount: res.test }
          ));
      }
    }

    return callAppleApi(receipt.apple_receipt, validation, callback);

  }

  validateSubscription(receipt: Receipt, callback: PromiseCallback) {

    var validation: ValidationCallback = (res) => {

      var maxExpiry = res.latest_receipt_info
        .filter(r => r.product_id === "co.insighttimer.feature.offlinepack")
        .sort((r1, r2) => parseInt(r2.expires_date_ms) - parseInt(r1.expires_date_ms)).shift().expires_date_ms;

      if (maxExpiry) {
        
        var response : ReceiptValidationResponse = {
          isValid: true, 
          isFound: true, 
          expiryDate: parseInt(maxExpiry), 
          isTestAccount: res.test 
        }

        var pending = res.pending_renewal_info.filter(r => r.product_id === "co.insighttimer.feature.offlinepack").shift();

        if(pending && pending.expiration_intent != null && pending.is_in_billing_retry_period == "0") {
          response.cancellationDate = parseInt(maxExpiry);
          response.cancelReason = cancelMap[pending.expiration_intent];
        }

        return callback(Promise.resolve(response))
      } else {
        return callback(
          Promise.resolve(
            { isValid: true, isFound: false, isTestAccount: res.test }
          )
        )
      }

    }

    return callAppleApi(receipt.apple_receipt, validation, callback);
  }

}


