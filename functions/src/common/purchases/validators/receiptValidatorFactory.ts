import { ReceiptValidator } from "./receiptValidators"
import { PurchasePlatform } from "../../../model/purchasesModel"

var validators = require("./receiptValidators");

function getValidator(platform: PurchasePlatform): ReceiptValidator {

  switch (platform) {
    case PurchasePlatform.APPLE:
      return validators.appleReceiptValidator;
    case PurchasePlatform.ANDROID:
      return validators.androidReceiptValidator;
    case PurchasePlatform.INSIGHT:
      return validators.defaultReceiptValidator;
    default:
      throw TypeError("Not Defined");
  }

}

module.exports.getValidator = getValidator;