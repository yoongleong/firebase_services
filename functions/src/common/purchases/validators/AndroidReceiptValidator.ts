import { Receipt } from "../../../model/receiptModel";
import { ReceiptValidationResponse } from "../receiptValidationResponse"
import { google, GoogleApis } from "googleapis"
import { PurchaseItemState } from "../../../model/purchasesModel";
import { AppConfig } from "../../../config/config";
import { ReceiptValidator, PromiseCallback } from "./receiptValidators"

let privatekey = require("./Insight Timer Production-1bc89d0c5cec.json")
let config: AppConfig = require("../../../config/config")

const cancelMap = {
  0: "User canceled the subscription",
  1: "Subscription was canceled by the system, for example because of a billing problem",
  2: "Subscription was replaced with a new subscription",
  3: "Subscription was canceled by the developer"
}

export class AndroidReceiptValidator implements ReceiptValidator {

  readonly jwtClient;
  readonly publisher;

  constructor() {

    console.log("initializing android validator");

    this.jwtClient = new google.auth.JWT(privatekey.client_email,
      null,
      privatekey.private_key,
      ["https://www.googleapis.com/auth/androidpublisher"]);

    this.publisher = google.androidpublisher({
      version: "v2",
      auth: this.jwtClient
    });

  }

  validatePurchase(receipt: Receipt, callback: PromiseCallback) {

    const params = {
      packageName: receipt.android_package_name,
      productId: receipt.android_product_id,
      token: receipt.android_token
    };

    var func = (err, res) => {
      if (err) {
        if (err.code && err.code >= 500) {
          // RETRY
          console.warn("Caught error validating receipt - " + err.code + ", " + err.message);
          return callback(Promise.reject(TypeError(err.code + ": " + err.message)));

        } else {
          console.warn("Caught error validating receipt - " + err);
          return callback(Promise.resolve({
            isValid: true,
            isFound: false,
            isTestAccount: res.data.purchaseType == 0 ? true : false,
          }));
        }
      }

      console.log(res.status + " " + JSON.stringify(res.data, null, 2));

      return callback(Promise.resolve({
        isValid: true,
        isFound: true,
        isTestAccount: res.data.purchaseType == 0 ? true : false,
      }));
    }

    this.publisher.purchases.products.get(params, null, func);
  }

  validateSubscription(receipt: Receipt, callback: PromiseCallback) {

    const params = {
      packageName: receipt.android_package_name,
      subscriptionId: receipt.android_subscription_id,
      token: receipt.android_token
    };

    var func = (err, res) => {
      if (err) {
        if (err.code && err.code >= 500) {
          // RETRY
          console.warn("Caught error validating receipt - " + err.code + ", " + err.message);
          return callback(Promise.reject(TypeError(err.code + ": " + err.message)));

        } else {
          console.warn("Caught error validating receipt - " + err);
          return callback(Promise.resolve({
            isValid: true,
            isFound: false,
            isTestAccount: res.data.purchaseType == 0 ? true : false,
          }));

        }
      }

      console.log(res.status + " " + JSON.stringify(res.data, null, 2));

      return callback(Promise.resolve({
        isValid: true,
        isFound: true,
        expiryDate: res.data.expiryTimeMillis,
        cancellationDate: res.data.userCancellationTimeMillis,
        cancelReason: cancelMap[res.data.cancelReason],
        isTestAccount: res.data.purchaseType == 0 ? true : false,
      }));
    }

    this.publisher.purchases.subscriptions.get(params, null, func);
  }

}
