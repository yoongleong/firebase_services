import { Receipt } from "../../../model/receiptModel";
import { ReceiptValidationResponse } from "../receiptValidationResponse"
import { AppleReceiptValidator } from "./AppleReceiptValidator"
import { AndroidReceiptValidator } from "./AndroidReceiptValidator"

export type PromiseCallback = (promise: Promise<ReceiptValidationResponse>) => any;

export interface ReceiptValidator {
  validateSubscription(receipt: Receipt, callback: PromiseCallback);
  validatePurchase(receipt: Receipt, callback: PromiseCallback);
}

class DefaultReceiptValidator implements ReceiptValidator {

  constructor() { }

  validatePurchase() {
    console.log("default validator");
  }

  validateSubscription() {

  }

}

module.exports.androidReceiptValidator = new AndroidReceiptValidator();
module.exports.appleReceiptValidator = new AppleReceiptValidator();
module.exports.defaultReceiptValidator = new DefaultReceiptValidator();