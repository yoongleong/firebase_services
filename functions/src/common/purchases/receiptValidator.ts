
import { Firestore, DocumentReference, DocumentSnapshot } from "@google-cloud/firestore";
import { ReceiptValidationResponse } from "./receiptValidationResponse";
import { Receipt } from "../../model/receiptModel";
import { UserPurchase } from "../../model/userPurchasesModel";
import { PurchasePlatform, PurchaseItemType } from "../../model/purchasesModel"

const fbUtil = require('../firebaseUtil');
const factory = require("./validators/receiptValidatorFactory")

function validateReceipt(receiptId: string, platformString: string, purchaseTypeString: string): Promise<ReceiptValidationResponse> {

  var fb: Firestore = fbUtil.getFirestore();
  var errors = [];

  return fb.collection('purchase_receipts').doc(receiptId).get()
    .then(doc => {
      if (!doc.exists) {
        throw Error("Document doesn't exist!");
      } else {
        const receipt: Receipt = new Receipt(doc.data());
        return this.validateReceiptContents(receipt, platformString, purchaseTypeString)
      }
    })
    .then(data => {


      var validator: ReceiptValidator = factory.getValidator();

      if(purchaseTypeString.startsWith("MELCHIOR_")) {
        const validator: ReceiptValidator = factory.getValidator(PurchasePlatform.APPLE);
      }

      return {
        isValid: true,
        isSubscription: (purchaseTypeString.startsWith("MELCHIOR_") ? true : false),
        isTestAccount: false
      }
    })
}

function validateReceiptContents(receipt: Receipt, platformString: string, purchaseTypeString: string): Promise<Receipt> {

  var errors = [];

  if (!receipt.platform)
    errors.push("platform");

  if (receipt.platform !== platformString)
    errors.push("platform doesn't match");

  var platform: PurchasePlatform = PurchasePlatform[platformString];
  var type: PurchaseItemType = PurchaseItemType[purchaseTypeString];

  switch (platform) {
    case PurchasePlatform.APPLE:

      if (!receipt.apple_receipt)
        errors.push("apple_receipt");

      if (!receipt.apple_transaction_id)
        errors.push("apple_transaction_id");

      break;

    case PurchasePlatform.ANDROID:

      if (!receipt.android_token)
        errors.push("android_token");

      if (!receipt.android_package_name)
        errors.push("android_package_name");

      switch (type) {
        case PurchaseItemType.MELCHIOR_1:
        case PurchaseItemType.MELCHIOR_2:
        case PurchaseItemType.MELCHIOR_3:
          if (!receipt.android_subscription_id)
            errors.push("android_subscription_id");
          break;
        case undefined:
            errors.push("invalid purchase type!");
          break;
        default:
          if (!receipt.android_product_id)
            errors.push("android_product_id");
          break;
      }
      break;

    case PurchasePlatform.INSIGHT:
      // TODO
      break;

    default:
      errors.push(`invalid purchase platform!`);
      break;
  }

  if (errors.length > 0) {
    return Promise.reject(TypeError(errors.toString()));
  }

  return Promise.resolve(receipt);
}

module.exports.validateReceiptContents = validateReceiptContents;
module.exports.validateReceipt = validateReceipt;