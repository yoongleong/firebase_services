const functions = require('firebase-functions');

var profile = functions.config().profile;

console.info("Current Profile : " + profile);

/**
 * Add new properties here to type properties within the configuration.
 * 
 * You may use as follows :
 *   eg. let config : AppConfig = require("../../config/config")
 * 
 * TODO :
 * - validation of typed properties?
 */
export class AppConfig {
  
  profile: string
  
  receiptValidation: {
    applePassword: string
  }
  
}

var config: AppConfig = new AppConfig();

var main = require("./appConfig.json");
var demo = require("./appConfig_demo.json");
var prod = require("./appConfig_prod.json");

if(profile && profile === "prod") {
  config = Object.assign(config, functions.config(), main, prod);
} else {
  config = Object.assign(config, functions.config(), main, demo);
}

console.log(JSON.stringify(config,null,2));

module.exports = config