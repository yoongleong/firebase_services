import * as glob from "glob";
import * as camelCase from "camelcase";

const fbUtil = require("./common/firebaseUtil");

fbUtil.initialize();

const files = glob.sync('./**/*.fun.js', { cwd: __dirname, ignore: './node_modules/**' });

console.info("Functions:");

for (let f = 0, fl = files.length; f < fl; f++) {
    const file = files[f];
    var names = file.slice(0,-7).split('/'); // Strip off '.fun.js'

    do {
      // remove './function'
      var folder = names.shift();
    } while (folder !== "function");
    
    const functionName = camelCase(names.join('_')); 
    console.info(file + " => " + functionName);
    if (!process.env.FUNCTION_NAME || process.env.FUNCTION_NAME === functionName) {
        exports[functionName] = require(file);
    }
}