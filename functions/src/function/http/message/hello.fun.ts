import * as functions from "firebase-functions"
import { Request, Response } from "express"

function get(req: Request, res: Response) {
  res.status(200).send("Hello World!");
}

function post(req: Request, res: Response) {
  res.status(202).send(`Hello ${req.body.value}`);
}

module.exports = functions.https.onRequest((req: Request, res: Response) => {
  switch (req.method) {
    case "GET":
      get(req, res);
      break;
    case "POST":
      post(req, res);
      break;
    default:
      res.status(405).send({ error: "Method Not Allowed" });
      break;
  }
});
