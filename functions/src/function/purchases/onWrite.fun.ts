import * as functions from "firebase-functions"
import { Event } from "firebase-functions";
import { DeltaDocumentSnapshot } from "firebase-functions/lib/providers/firestore";

function validateContents() {
  
}

function validateReceipt() {
  
}

function writePurchase() {
  
}

module.exports = functions.firestore
  .document('user_purchases/{userId}/{purchaseCollectionId}/{purchaseId}')
  .onCreate((event: Event<DeltaDocumentSnapshot>) => {
    var newValue = event.data.data();

    validateContents();
    validateReceipt();
    writePurchase();

  });