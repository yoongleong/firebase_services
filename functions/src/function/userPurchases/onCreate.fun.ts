import * as functions from "firebase-functions"
import { Event } from "firebase-functions";
import { DeltaDocumentSnapshot } from "firebase-functions/lib/providers/firestore";

import { UserPurchase } from "../../model/userPurchasesModel"

var validator = require("./validator")
var receiptValidator = require("../../common/purchases/receiptValidator")

/**
 * todo: 
 * - for all functions, should we set flags so we know they have been updated ?
 * - build purchase
 * - write purchase to purchases
 */
module.exports = functions.firestore
  .document('user_purchases/{userId}/{purchaseCollectionId}/{purchaseId}')
  .onCreate((event: Event<DeltaDocumentSnapshot>) => {

    // get data
    var userPurchase : UserPurchase = new UserPurchase(event.data.data());

    // logging
    console.info(`handling [${event.params.userId}:${event.params.purchaseId}] - test ` + JSON.stringify(event.data.data(), null, 2));

    return validator.validateContents(userPurchase)
      .then(resp => receiptValidator.validateReceipt(userPurchase.receipt_id, userPurchase.platform, userPurchase.item_type))
      .then(resp => {
        console.log("valid receipt ? " + resp.isValid );
      })
      .catch(err => {
        console.warn("error " + err);
      })

  });
  