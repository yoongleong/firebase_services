/**
 * @returns promise.resolve(true) if successful OR
 *          promise.reject(error : string) if failed
 */
function validateContents(record) : Promise <boolean> {
  
  var errors = [];

  if( !record.platform ) 
    errors.push("platform");

  if( !record.local_currency ) 
    errors.push("local_currency");

  if( !record.item_type ) 
    errors.push("item_type");

  if( !record.items ) 
    errors.push("items");

  if( !record.purchased_at ) 
    errors.push("purchased_at");

  if( !record.created_at ) 
    errors.push("created_at");
  
  if( errors.length > 0 ) {
    return Promise.reject(TypeError(errors.toString()));
  }

  return Promise.resolve(true);
}

module.exports.validateContents = validateContents;
