import "mocha"
import sinon = require("sinon")
import assert = require("assert")
import { Request, Response } from "express"


describe('test httpMessageHello', function () {

  var fun, fbUtil;

  before(() => {
    fun = require('../../../src/functions/http/message/hello.fun');
  });
  
  after(() => {
  });

  it('test GET', function () {
    const req = { method: "GET" };
    const res = {
      status: (code) => {
        assert.equal(code, 200);
        return res;
      },
      send: (str) => {
        assert.equal(str, "Hello World!");
        return res;
      }
    }
    fun(req, res);
  });

  it('test POST', function () {
    const req = { method: "POST", body: { value: "Yoong" } };
    const res = {
      status: (code) => {
        assert.equal(code, 202);
        return res;
      },
      send: (str) => {
        assert.equal(str, "Hello Yoong");
        return res;
      }
    }
    fun(req, res);
  });

  it('test negative', function () {
    const req = { method: "PUT" };
    const res = {
      status: (code) => {
        assert.equal(code, 405);
        return res;
      },
      send: (r) => {
        assert.equal(r.error, "Method Not Allowed");
        return res;
      }
    }
    fun(req, res);
  });

});
