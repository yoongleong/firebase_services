import "mocha"
import sinon = require("sinon")
import { UserPurchase } from "../../../src/model/userPurchasesModel"

import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");

describe('test userPurchase/validator.validateContents', () => {

  var fun;

  before(() => {
    chai.use(chaiAsPromised);
    fun = require("../../../src/functions/userPurchases/validator");
  });

  it('test plainObject pass', () => {

    var data = {
      platform: "APPLE",
      local_currency: "RM2.99",
      item_type: "MELCHIOR_1",
      items: ["OFFLINE"],
      purchased_at: Date.now(),
      created_at: Date.now(),
    };

    return chai.expect(fun.validateContents(data)).to.eventually.be.equal(true);
  });

  it('test plainObject fail', () => {

    var data = {
      local_currency: null,
      item_type: "MELCHIOR_1",
      items: ["OFFLINE"],
      purchased_at: Date.now(),
      created_at: Date.now(),
    };

    return chai.expect(fun.validateContents(data))
      .to.eventually.be.rejectedWith(TypeError)
      .and.have.property('message')
      .that.include("local_currency")
      .and.include("platform");

  });

  it('test UserPurchaseObject fail', () => {

    var data = {
      platform: "APPLE",
      local_currency: null,
      item_type: "MELCHIOR_1",
      items: ["OFFLINE"],
      purchased_at: Date.now()
    };

    return chai.expect(fun.validateContents(data))
      .to.eventually.be.rejectedWith(TypeError)
      .and.have.property('message')
      .that.include("local_currency")
      .and.include("created_at");

  });

});
