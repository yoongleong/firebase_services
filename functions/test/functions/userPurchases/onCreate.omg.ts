import "mocha"
import sinon = require("sinon")
import { DeltaDocumentSnapshot } from "firebase-functions/lib/providers/firestore";
import * as functions from "firebase-functions"

describe('test onCreate', function () {

  var fun;

  before(() => {
    fun = require("./onCreate.fun");
  });

  it('test onCreate', function () {

    const fakeEvent = {
      
      data: new functions.database.DeltaSnapshot(null, null, null, 'input')
      
    };

    var validator = require('../../src/userPurchases/validator');
    var receiptValidator = require('../../src/userPurchases/receiptValidator');

    var validateContentStub = sinon.stub(validator, 'validateContents').returns(
      Promise.resolve(true)
    );

    var validateReceiptStub = sinon.stub(receiptValidator, '').returns(
      Promise.resolve({
        isValid: true,
        isSubscription: true,
        isTestAccount: false
      })
    );

    fun(fakeEvent);

  });

});
