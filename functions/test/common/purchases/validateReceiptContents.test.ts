import "mocha"
import sinon = require("sinon")
import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { Receipt } from "../../../src/model/receiptModel";

describe('test validateReceiptContents', () => {

  var fun;

  before(() => {
    chai.use(chaiAsPromised);
    fun = require("../../../src/common/purchases/receiptValidator");
  });

  it('test Apple Receipt', () => {

    var data = {
      platform: "APPLE",
      apple_receipt: "receipt",
      apple_transaction_id: "123",
    };

    return chai.expect(fun.validateReceiptContents(data, "APPLE", "MELCHIOR_1")).to.eventually.be.equal(true);
  });

  it('test APPLE invalid Receipt - empty field', () => {

    var data = {
      platform: "APPLE",
      apple_receipt: "receipt",
      apple_transaction_id: "",
    };

    return chai.expect(fun.validateReceiptContents(data, "APPLE", "COURSE_PURCHASE_10_DAY"))
      .to.eventually.be.rejectedWith(TypeError)
      .and.have.property('message')
      .that.include("apple_transaction_id");
  });

  it('test Android Sub Receipt', () => {

    var data = {
      platform: "ANDROID",
      android_token: "token",
      android_package_name: "package",
      android_subscription_id: "sub_id"
    };

    return chai.expect(fun.validateReceiptContents(data, "ANDROID", "MELCHIOR_1")).to.eventually.be.equal(true);
  });

  it('test Android Purchase Receipt', () => {

    var data = {
      platform: "ANDROID",
      android_token: "token",
      android_package_name: "package",
      android_subscription_id: "sub_id",
      android_product_id: "product_id"
    };

    return chai.expect(fun.validateReceiptContents(new Receipt(data), "ANDROID", "COURSE_PURCHASE_10_DAY")).to.eventually.be.equal(true);
  });

  it('test Android invalid Receipt - missing field', () => {

    var data = {
      platform: "ANDROID",
      android_token: "token",
      android_package_name: "package",
      android_product_id: "product_id"
    };

    return chai.expect(fun.validateReceiptContents(data, "ANDROID", "MELCHIOR_1"))
      .to.eventually.be.rejectedWith(TypeError)
      .and.have.property('message')
      .that.include("android_subscription_id");
  });

  it('test Invalid Type', () => {

    var data = {
      platform: "ANDROID",
      android_token: "token",
      android_package_name: "package",
      android_subscription_id: "sub_id",
    };

    return chai.expect(fun.validateReceiptContents(data, "ANDROID", "DOESNT_EXIST"))
      .to.eventually.be.rejectedWith(TypeError)
      .and.have.property('message')
      .that.include("invalid purchase type");
  });

  it('test Invalid Platform', () => {

    var data = {
      platform: "INVALID"
    };

    return chai.expect(fun.validateReceiptContents(data, "INVALID", "COURSE_PURCHASE_10_DAY"))
      .to.eventually.be.rejectedWith(TypeError)
      .and.have.property('message')
      .that.include("invalid purchase platform");
  });

});
