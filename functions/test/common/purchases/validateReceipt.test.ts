import "mocha"
import sinon = require("sinon");
import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { Receipt } from "../../../src/model/receiptModel";


describe('test validateReceipt', () => {

  var fun, fbutil, fbStub, funStub;

  before(() => {
    chai.use(chaiAsPromised);
    fbutil = require("../../../src/common/firebaseUtil");
    fun = require("../../../src/common/purchases/receiptValidator");

    fbStub = sinon.stub(fbutil, "getFirestore")
    funStub = sinon.stub(fun, "validateReceiptContents")
  });

  it('test valid Receipt', () => {

    var doc = {
      exists : true,
      data : function() { 
        return { 
          platform : "INSIGHT"
        } 
      }
    }

    var firestore = {
      collection: function () {
        return { doc: function () {
            return { get: function () { 
              return Promise.resolve(doc); 
            }
        }}
      }}
    }

    fbStub.returns(firestore);
    funStub.returns(Promise.resolve(true));

    return chai.expect(fun.validateReceipt("RECEIPT_ID", "APPLE", "MELCHIOR_1"))
      .to.eventually.have.property("isValid").that.equal(true);

  });

  it('test invalid Receipt', () => {

    var doc = {
      exists : false
    }

    var firestore = {
      collection: function () {
        return { doc: function () {
            return { get: function () { 
              return Promise.resolve(doc); 
            }
        }}
      }}
    }

    fbStub.returns(firestore);

    return chai.expect(fun.validateReceipt("RECEIPT_ID", "APPLE", "MELCHIOR_1")).to.eventually.be.rejectedWith(Error);

  });

});
