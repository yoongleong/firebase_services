import "mocha"
import sinon = require("sinon")
import chai = require("chai")
import { AppConfig } from "../../src/config/config";

// TODO: update tests so it doesn't depend on actual config files..

describe('test configuration', () => {

  var cfgStub, firebaseCfg;

  beforeEach(() => {

    Object.keys(require.cache).forEach((key) => { 
      delete require.cache[key] 
    })

    firebaseCfg = require('firebase-functions');
    cfgStub = sinon.stub(firebaseCfg, "config");

  });
  
  it('test demo', () => {

    cfgStub.returns({ profile: "demo" });

    var cfg = require("../../src/config/config");

    chai.assert.equal(cfg.profile, "demo");
    chai.assert.equal(cfg.config, "demo");
    chai.assert.equal(cfg.main_config, true);
    chai.assert.equal(cfg.demo, true);

  });

  it('test prod', () => {

    cfgStub.returns({ profile: "prod" });

    var cfg = require("../../src/config/config");

    chai.assert.equal(cfg.profile, "prod");
    chai.assert.equal(cfg.config, "prod");
    chai.assert.equal(cfg.main_config, true);
    chai.assert.equal(cfg.prod, true);

  });

  it('test default', () => {

    cfgStub.returns({});

    var cfg = require("../../src/config/config");

    chai.assert.equal(cfg.profile, undefined);
    chai.assert.equal(cfg.config, "demo");
    chai.assert.equal(cfg.main_config, true);
    chai.assert.equal(cfg.demo, true);

  });

});
